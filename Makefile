CFLAGS = -std=c11

all:
	+$(MAKE) -C src/main CFLAGS=$(CFLAGS)
	cp src/main/imgFind.o imgFind

debug: CFLAGS := '$(CFLAGS) -g -Wall'
debug: all

test: CFLAGS := '$(CFLAGS) -g -Wall'
test: debug
	+$(MAKE) -C src/test CFLAGS=$(CFLAGS)

clean:
	+$(MAKE) -C src/main clean
	+$(MAKE) -C src/test clean
	$(RM) imgFind
