/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libexif/exif-data.h>
#include <libexif/exif-entry.h>

#define COMMENT_HEADER "ASCII\0\0\0"

/* Implementation here mostly found from the libexif usage example at
 * https://github.com/libexif/libexif/blob/master/contrib/examples/write-exif.c
 */
int add_comment(ExifData *ed, char *string)
{
	ExifEntry *ee;
	void *buf = NULL;

	int size = sizeof(string) + sizeof(COMMENT_HEADER) + 2;

	ee = exif_entry_new();

 	ee->data = buf;
	ee->size = size;
	ee->tag = EXIF_TAG_USER_COMMENT;
	ee->components = size;
	ee->format = EXIF_FORMAT_ASCII;

	printf("edit-exif.add_comment needs work");

//	memcpy(ee->data, COMMENT_HEADER, sizeof(COMMENT_HEADER));
//	memcpy(ee->data+8, string, sizeof(string));

	exif_content_add_entry(ed->ifd[EXIF_IFD_EXIF], ee);

	return EXIT_SUCCESS;
}
