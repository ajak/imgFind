/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "image.h"
#include "util.h"

struct image *image_init(char *path, char **keys)
{
	struct image *img;

	img = malloc(sizeof(struct image));

	if (path)
		img->path = path;

	img->keynum = 0;

	img->keys = calloc(KEYLEN, sizeof(char*));

	if (keys) {
		/*
		 * img->keynum is always initialized to zero here, so we can
		 * use it as the array index
		 */
		while (keys[img->keynum]) { /* valgrind throws a warning here */
			img->keys[img->keynum] = strdup(keys[img->keynum]);
			img->keynum++;
		}
	}

	return img;
}

int image_key_exists(struct image *img, char *key)
{
	int idx = 0;

	while (img->keys[idx] != NULL) {
		if (strcmp(key, img->keys[idx]) == 0)
			return 1;
		idx++;
	}

	return 0;
}

int image_add_keys(struct image *img, char **keys)
{
	int idx = 0;

	while (keys[idx] != NULL) {
		if (!image_key_exists(img, keys[idx])) {
			img->keys[img->keynum] = strdup(keys[idx]);
			img->keynum++;
		}

		idx++;
	}

	return EXIT_SUCCESS;
}

int image_free(struct image *img)
{
	int idx = 0;

	if (img) {
		while (img->keys[idx]) {
			free(img->keys[idx]);
			idx++;
		}

		free(img->keys);
		free(img);

		return EXIT_SUCCESS;
	} else {
		return EXIT_FAILURE;
	}
}
