/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <stdlib.h>
#include <json-c/json_object.h>

#include "json-image.h"
#include "image.h"
#include "util.h"

/* Add keys from the image object to the json data */
int json_add_keys(json_object *json_img, struct image *img)
{
	json_object *key_array;
	int keylen = 0;

 	key_array = json_object_new_array();

	if (!key_array)
		return EXIT_FAILURE;

	while (img->keys[keylen] != NULL) {
		json_object_array_add(key_array,
				json_object_new_string(img->keys[keylen]));
		keylen++;
	}

	json_object_object_add(json_img, "keys", key_array);

	return EXIT_SUCCESS;
}

struct json_object *json_image_create(struct image *img)
{
	json_object *json_img;
	json_object *key_arr;
	int i;

	json_img = json_object_new_object();
	key_arr = json_object_new_array();

	json_object_object_add(json_img, "path", json_object_new_string(img->path));

	for (i = 0; i < img->keynum; i++)
		json_object_array_add(key_arr,
				json_object_new_string(img->keys[i]));

	json_object_object_add(json_img, "keys", key_arr);

	return json_img;
}
