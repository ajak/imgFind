/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <json-c/json_object.h>
#include <json-c/json_tokener.h>
#include <json-c/json_util.h>
#include <libexif/exif-data.h>

#include "image.h"
#include "json-file.h"
#include "json-image.h"
#include "util.h"

long get_file_size(FILE *fp)
{
	long size;

	fseek(fp, 0L, SEEK_END);
	size = ftell(fp);
	rewind(fp);

	return size;
}

struct json_object *get_json_data(FILE *fp)
{
	json_object *jo;
	char *buf;
	long size;

	if (fp) {
		size = get_file_size(fp);

		if (size != 0L) { /* file isn't empty, try to read */
			buf = malloc(size + 1);

			fread(buf, size, 1, fp);
			buf[size] = 0;

			jo = json_tokener_parse(buf);

			free(buf);

			return jo;
		} else { /* file is empty */
			goto handle_bad_file;
		}
	} else {
		goto handle_bad_file;
	}

handle_bad_file:
	jo = json_object_new_object();

	json_object_object_add(jo, "images", json_object_new_array());

	return jo;
}

int write_json(struct jsonfile *jf)
{
	FILE *fp;
	const char *buf;

	if (jf->path && jf->jo) {
		buf = json_object_to_json_string(jf->jo);

		fp = fopen(jf->path, "w");
		fputs(buf, fp);

		fclose(fp);

		return EXIT_SUCCESS;
	} else {
		return EXIT_FAILURE;
	}
}

struct json_object *get_json_image_array(json_object *jo)
{
	json_object *img_arr;

	json_object_object_get_ex(jo, "images", &img_arr);
	json_object_get(img_arr);

	if (img_arr)
		return img_arr;
	else
		return 0;
}

/* Using json_object_array_bsearch may be quicker than this implementation */
int json_image_exists(json_object *root_jo, char *img_path)
{
	json_object *img_arr;
	json_object *img_json;
	json_object *path;
	int i;

	img_arr = get_json_image_array(root_jo);

	for (i = 0; i < json_object_array_length(img_arr); i++) {
		img_json = json_object_array_get_idx(img_arr, i);
		json_object_object_get_ex(img_json, "path", &path);
		json_object_get(path);

		if (strcmp(json_object_get_string(path), img_path) == 0) {
			json_object_put(path);
			json_object_put(img_arr);
			return TRUE;
		} else {
			json_object_put(img_json);
			json_object_put(path);
		}
	}

	json_object_put(img_arr);

	return FALSE;
}

int add_image(struct jsonfile *jf, char *path, char **keys)
{
	json_object *img_arr;
	json_object *json_img;

	if (jf) {
		jf->i[jf->size] = image_init(path, keys);

		img_arr = get_json_image_array(jf->jo); // img_arr->_ref_count++;

		/* If the JSON data already has the image, do nothing else */
		if (json_image_exists(jf->jo, path))
			return EXIT_SUCCESS;

		json_img = json_image_create(jf->i[jf->size]);
		json_add_keys(json_img, jf->i[jf->size]);

		json_object_array_add(img_arr, json_img);
		json_object_object_add(jf->jo, "images", img_arr);

		jf->size++;
		return EXIT_SUCCESS;
	} else {
		return EXIT_FAILURE;
	}
}

struct json_object *get_json_image(struct jsonfile *jf, char *img_path)
{
	json_object *img_arr;
	json_object *img_json;
	json_object *path;
	int i;

	img_arr = get_json_image_array(jf->jo);

	for (i = 0; i < json_object_array_length(img_arr); i++) {
		img_json = json_object_array_get_idx(img_arr, i);
		json_object_object_get_ex(img_json, "path", &path);
		json_object_get(path);

		if (strcmp(json_object_get_string(path), img_path) == 0) {
			json_object_put(path);
			json_object_put(img_arr);
			return img_json;
		} else {
			json_object_put(img_json);
			json_object_put(path);
		}
	}

	json_object_put(img_arr);

	return FALSE;
}

struct image *get_image(struct jsonfile *jf, char *img_path)
{
	int i;

	for (i = 0; i < jf->size + 1; i++)
		if (strcmp(jf->i[i]->path, img_path) == 0)
			return jf->i[i];

	return FALSE;
}

struct image *json_image_to_image(struct json_object *img_json)
{
	json_object *path;
	json_object *key_arr;
	json_object *key;
	struct image *img;
	char **keys = NULL;
	int key_arr_len;
	int i;

	json_object_object_get_ex(img_json, "path", &path);
	json_object_object_get_ex(img_json, "keys", &key_arr);

	key_arr_len = json_object_array_length(key_arr);

	/* TODO - fix char** handling here too */
	if(key_arr_len != 0)
		keys = calloc(key_arr_len, sizeof(char*) + 1);

	for (i = 0; i < key_arr_len; i++) {
		key = json_object_array_get_idx(key_arr, i);

		keys[i] = (char*)json_object_get_string(key);
	}

	img = image_init((char*)json_object_get_string(path), keys);

	if(keys)
		free(keys);

	return img;
}

struct jsonfile *json_file_init(char *path)
{
	struct jsonfile *jf;
	json_object *img_arr;
	FILE *fp;
	int i;

	fp = fopen(path, "r");

	jf = malloc(sizeof(struct jsonfile));
	jf->jo = get_json_data(fp);

	jf->path = path;
	jf->size = 0;

	img_arr = get_json_image_array(jf->jo);

	for (i = 0; i < json_object_array_length(img_arr); i++) {
		jf->i[i] = json_image_to_image(json_object_array_get_idx(img_arr, i));
		jf->size++;
	}

	json_object_put(img_arr);

	if (fp)
		fclose(fp);

	return jf;
}

/*
 *
 * JSON file should always be freed using this function at the end
 * of its use to write the JSON data to the file.
 *
 */
int json_file_free(struct jsonfile *jf)
{
	int i;

	write_json(jf);

	json_object_put(jf->jo);

	for (i = 0; i < jf->size; i++)
		image_free(jf->i[i]);

	free(jf);

	return EXIT_SUCCESS;
}
