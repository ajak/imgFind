/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <magic.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include "util.h"

/* This assumes the array is terminated with a NULL array member */
int string_contains(char *str, char **array)
{
	int idx = 0;

	/* Worst-case O(n) isn't very good */
	while (array[idx]) {
		if(strcmp(str, array[idx]) == 0)
			return 1;
		idx++;
	}

	return 0;
}

int file_exists(char *path)
{
	return (access(path, F_OK) != -1 );
}

/* Using supported_types like this is sloppy */
int is_supported(char *path)
{
	/* Possible string values of mimetype below */
	static const char *supported_types = "jpeg/jpg/jpe/jfif";
	const char *mimetype;
	magic_t magic;

	magic = magic_open(MAGIC_EXTENSION);
	magic_load(magic, NULL);
	mimetype = magic_file(magic, path);

	if (strstr(supported_types, mimetype)) {
		magic_close(magic);
		return 1;
	} else {
		magic_close(magic);
		return 0;
	}
}

/* With help from https://stackoverflow.com/q/32944390 */
char *strdup(char *str)
{
	int size;
	char *ret;

	if (str) {
		size = strlen(str) + 1;
		ret = malloc(size);

		if (ret) {
			memcpy(ret, str, size);
			return ret;
		}
	}

	return NULL;
}
