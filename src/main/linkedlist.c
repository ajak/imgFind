/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <stdlib.h>

#include "datastructs.h"

struct ll_node *ll_create()
{
	struct ll_node *node = malloc(sizeof(struct ll_node));

	node->next = NULL;
	node->data = NULL;

	return node;
}

int ll_exists(struct ll_node *list_node, void *data)
{
	if (!data)
		return 0;

	while(1) {
		if (list_node->data == data)
			return 1;

		list_node = list_node->next;

		if(!list_node)
			break;
	}

	return 0;
}

/* First nodes of the list must be externally malloc'd with ll_create */
int ll_insert(struct ll_node *list_node, void *data)
{
	struct ll_node *node;

	if (!list_node) {
		return EXIT_FAILURE;
	} else {
		if (!list_node->data) { // list_node is an empty node
			list_node->data = data;

			return EXIT_SUCCESS;
		} else if (!list_node->next) { // list_node must be the tail node
			node = malloc(sizeof(struct ll_node));

			node->next = NULL;
			node->data = data;

			list_node->next = node;

			return EXIT_SUCCESS;
		} else { // list_node must be something else
			node = malloc(sizeof(struct ll_node));

			node->data = data;

			/* Rather than looping through the rest of the list, we
			   set the new node's next to the next of the given node
 			   and set the given node's next to our new node */

			node->next = list_node->next;
			list_node->next = node;

			return EXIT_SUCCESS;
		}
	}

	return EXIT_FAILURE;
}

/* The node preceding the node to be deleted must be passed as the argument here
 * to adjust the next pointer of that node.
 */
int ll_node_delete(struct ll_node *prev_node)
{
	struct ll_node *node = prev_node->next; // The node to delete

	if (!prev_node || !prev_node->next)
		return EXIT_FAILURE;

	if (!node->next) { // Deleting a tail node
		prev_node->next = NULL;

		node->data = NULL;
		free(node);

		return EXIT_SUCCESS;
	} else if (node->next) { // Deleting a middle node
		prev_node->next = node->next;

		node->data = NULL;
		free(node);

		return EXIT_SUCCESS;
	}

	return EXIT_FAILURE;
}

/* Loops through the given list and deletes the list node matching given data,
 * if one is found.
 *
 * Returns a pointer to the head node of the list. If the list is empty after
 * the delete operation, NULL is returned.
 */
struct ll_node *ll_delete(struct ll_node *head, void *data)
{
	struct ll_node *node = NULL;

	if (head->data == data) { // Delete the head node
		if (head->next) {
			// If it isn't the only node, adjust the head pointer
			node = head->next;

			free(head);

			head = node;

			return head;
		} else {
			// We deleted the only node in the list, free and return
			free(head);

			return NULL;
		}
	}

	// Loop through the rest of the list to find what we're looking for
	node = head;

	while(1) {
		/* Need to examine next's data so we can pass the node preceding
		   the node to delete to ll_node_delete */
		if (node->next->data == data)
			if (ll_node_delete(node) == EXIT_SUCCESS)
				return head;

		node = node->next;

		if(!node || !node->next)
			break;
	}

	return head;
}

int ll_free(struct ll_node *head)
{
	struct ll_node *node;

	while((node = head) != NULL) {
		head = head->next;
		free(node);
	}

	return EXIT_SUCCESS;
}
