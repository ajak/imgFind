/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <stdlib.h>

#include "datastructs.h"
#include "util.h"

// djb2 from http://www.cse.yorku.ca/~oz/hash.html
unsigned long hash(char *str)
{
	unsigned long hash = 5381;
	int c;

	while ((c = *str++))
		hash = ((hash << 5) + hash) + c;

	return hash;
}

struct ht_node *hash_search(struct ht_node **ht, char *key)
{
	// hash produces massive numbers, modulus it to get it under 1024
	unsigned long idx = hash(key) % LENGTH;

	return (struct ht_node*) NULL;
}

int hash_insert(struct ht_node **ht, char *key, void *data)
{
	struct ht_node *item;
	unsigned long idx = hash(key) % LENGTH;

	if (!ht[idx]) {
		item = malloc(sizeof(struct ht_node));

		if (item) {
			item->key = strdup(key);
			item->ll_head = ll_create();
		} else {
			return EXIT_FAILURE;
		}

		ht[idx] = item;
	} else {
		item = ht[idx];
	}

	printf("Data inserted at index %d\n", idx);
	if (ht[idx]->ll_head) // The linked list head should always be here
		ll_insert(ht[idx]->ll_head, data);

	return EXIT_SUCCESS;
}

int hash_delete(struct ht_node **ht, struct ht_node *item)
{
	return EXIT_SUCCESS;
}

struct ht_node **hash_init()
{
	struct ht_node **ht;

	ht = calloc(LENGTH, sizeof(struct ht_node));

	return ht;
}

int ht_node_free(struct ht_node *ht)
{
	if(ht) {
		if (ht->key)
			free(ht->key);
		if (ht->ll_head)
			ll_free(ht->ll_head);

		free(ht);
	}

	return EXIT_SUCCESS;
}

int hash_free(struct ht_node **ht)
{
	int idx;

	for(idx = 0; idx < 1024; idx++) {
		if (ht[idx]) {
			ht_node_free(ht[idx]);
		}
	}

	free(ht);

	return EXIT_SUCCESS;
}
