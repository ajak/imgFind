/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <stdlib.h>
#include <string.h>

#include "data.h"
#include "datastructs.h"
#include "image.h"
#include "json-file.h"
#include "json-image.h"
#include "main.h"
#include "util.h"

#define DEFAULT_JSON_PATH "/.local/var/imgfind.json"

struct opts o;
struct jsonfile *jf;

int verify_jsonfile()
{
	if (!jf) {
		if (!o.jf_path)
			o.jf_path = strcat(getenv("HOME"), DEFAULT_JSON_PATH);
		else if (o.jf_path) {
			jf = json_file_init(o.jf_path);
		}

		jf = json_file_init(o.jf_path);
	}

	if (jf)
		return EXIT_SUCCESS;
	else
		return EXIT_FAILURE;
}

int json_del_keys(char *img_path, char **keys)
{
	json_object *json_image;
	json_object *key_array;
	json_object *new_keys;
	json_object *key;
	int idx;

	if (verify_jsonfile() != EXIT_SUCCESS)
		return EXIT_FAILURE;

	json_image = get_json_image(jf, img_path);

	json_object_object_get_ex(json_image, "keys", &key_array);
	json_object_get(key_array);

	new_keys = json_object_new_array();

	/* loop through key_arr and delete the key_arr[idx] if it exists in keys */
	for (idx = 0; idx < json_object_array_length(key_array); idx++) {
		key = json_object_array_get_idx(key_array, idx);

		if(!string_contains((char*) json_object_get_string(key), keys))
			json_object_array_add(new_keys, key);
	}

	json_object_object_add(json_image, "keys", new_keys);

	json_object_put(key_array);

	return EXIT_SUCCESS;
}

int image_del_keys(char *img_path, char **keys)
{
}

int data_append_keys(char *img_path, char **keys)
{
	struct image *img;
	struct json_object *img_json;

	if (verify_jsonfile() != EXIT_SUCCESS)
		return EXIT_FAILURE;

	img = get_image(jf, img_path);
	img_json = get_json_image(jf, img_path);

	image_add_keys(img, keys);

	json_add_keys(img_json, img);

	return EXIT_SUCCESS;
}

int data_del_keys(char *img_path, char **keys)
{
	if (json_del_keys(img_path, keys) == EXIT_SUCCESS)
		return EXIT_SUCCESS;
}

char **data_get_image_keys(char *img_path)
{
	struct image *img;

	if (verify_jsonfile() == EXIT_SUCCESS) {
		img = get_image(jf, img_path);
		return img->keys;
	}
}

int data_load()
{
	if (verify_jsonfile() == EXIT_SUCCESS)
		return EXIT_SUCCESS;

	return EXIT_FAILURE;
}

int data_save()
{
	if (verify_jsonfile() == EXIT_SUCCESS)
		return json_file_free(jf);

	return EXIT_FAILURE;
}
