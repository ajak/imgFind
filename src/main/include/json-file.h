/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef JSON_FILE_H
#define JSON_FILE_H

#include <json-c/json_object.h>

struct jsonfile {
	char *path;
	struct json_object *jo;
	int size;
	struct image *i[1024]; /* TODO - dynamic array sizing */
};

/* Implemented in json-file.c */
int json_image_exists(json_object *root_jo, char *img_path);
int add_image(struct jsonfile *jf, char *path, char **keys);
struct json_object *get_json_image(struct jsonfile *jf, char *img_path);
struct image *get_image(struct jsonfile *jf, char *img_path);
struct jsonfile *json_file_init(char *path);
int json_file_free(struct jsonfile *jf);

#endif /* JSON_FILE_H */
