/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef EXIF_H
#define EXIF_H

#include <libexif/exif-data.h>

/* Implemented in read-exif.c */
int get_exif_data(ExifData **ed, char *file);

/* Implemented in edit-exif.c */
void add_comment(ExifData *ed, char *string);

#endif /* EXIF_H */
