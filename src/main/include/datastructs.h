/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef DATASTRUCTS_H
#define DATASTRUCTS_H

#define LENGTH 1024

struct ll_node {
	void *data;
	struct ll_node *next;
};

struct ht_node {
	char *key;
	struct ll_node *ll_head;
};

/* Implemented in linkedlist.c */
struct ll_node *ll_create();
int ll_exists(struct ll_node *list_node, void *data);
int ll_insert(struct ll_node *list_node, void *data);
struct ll_node *ll_delete(struct ll_node *head, void *data);
int ll_free(struct ll_node *head);

/* Implemented in hashtable.c */
struct ht_node *hash_search(struct ht_node **ht, char *key);
int hash_insert(struct ht_node **ht, char *key, void *data);
int hash_delete(struct ht_node **ht, struct ht_node *item);
struct ht_node **hash_init();
int hash_free(struct ht_node **ht);

#endif
