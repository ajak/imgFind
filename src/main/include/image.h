/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef IMAGE_H
#define IMAGE_H

#include <libexif/exif-data.h>

#define KEYLEN 1024

struct image {
	char *path;
	int keynum;
	char **keys;
	ExifData *ed;
};

/* Implemented in image.c */
struct image *image_init(char *path, char **keys);
int image_add_keys(struct image *img, char **keys);
int image_free(struct image *img);

#endif /* IMAGE_H */
