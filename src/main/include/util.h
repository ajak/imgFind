/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef IMGFIND_UTIL_H
#define IMGFIND_UTIL_H

#define arrlen(x) (sizeof(x) / sizeof((x)[0]))

/* TRUE and FALSE are currently used only by files that already include
 * json_object.h, so it suffices to use those booleans
 */
//#define TRUE 1
//#define FALSE 0

/* Implemented in util.c */
int string_contains(char *str, char **array);
int file_exists(char *path);
int is_supported(char *path);
char *strdup(char *str);

#endif /* IMGFIND_UTIL_H */
