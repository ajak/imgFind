/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libexif/exif-data.h>
#include <json-c/json_object.h>

#include "data.h"
#include "exif.h"
#include "image.h"
#include "json-file.h"
#include "json-image.h"
#include "main.h"
#include "util.h"

/*
 * Magic numbers to direct execution after parsing command line arguments,
 * see main()
 */
#define ADD_KEY   1
#define DEL_KEY   2
#define LIST_KEYS 3
#define SEARCH    4
#define HELP      5

char **get_keys(int argc, char **argv)
{
	char **keys;
	int idx = 0;
	int keylen = 0;

	idx = optind - 1;
	keys = calloc(32, sizeof(char*));

	while(idx < argc) {
		if (argv[idx][0] != '-') {
			keys[keylen] = argv[idx];
			keylen++;
		} else {
			break;
		}

		idx++;
	}

	optind = idx - 1;

	return keys;
}

int check_image(char *img_path)
{
	/* Make sure the path given for the image is sane */
	if (!img_path) {
		printf("No image given. Exiting.\n");
		return EXIT_FAILURE;
	} else if (!file_exists(img_path)) {
		printf("Image at '%s' doesn't exist. Exiting.\n", img_path);
		return EXIT_FAILURE;
	} else if (!is_supported(img_path)) {
		printf("%s isn't a supported format. Exiting.\n", img_path);
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

int add_keys(char *img_path, char **keys, char *jf_path)
{
	if(data_load() != EXIT_SUCCESS || check_image(img_path) != EXIT_SUCCESS)
		goto exit_error;

	if (!json_image_exists(jf->jo, img_path)) {
		add_image(jf, img_path, keys);
	} else {
		data_append_keys(img_path, keys);
	}

	/* Clean up */
	free(keys);
	data_save();

	return EXIT_SUCCESS;

exit_error:
	free(keys);
	data_save();

	return EXIT_FAILURE;
}

int list_keys(char *img_path, char *jf_path)
{
	char **keys;
	int idx = 0;

	if (data_load() != EXIT_SUCCESS)
		return EXIT_FAILURE;

	if (check_image(img_path) == EXIT_SUCCESS) {
		keys = data_get_image_keys(img_path);

		while (keys[idx]) {
			printf("%s\n", keys[idx]);
			idx++;
		}
	}

	data_save();

	return EXIT_SUCCESS;
}

int del_keys(char *img_path, char **keys, char *jf_path)
{
	if(data_load() != EXIT_SUCCESS || check_image(img_path) != EXIT_SUCCESS)
		goto exit_error;

	if (!json_image_exists(jf->jo, img_path)) {
		/* Can't delete keys if the image has no keys, exit */
		return EXIT_SUCCESS;
	} else {
		data_del_keys(img_path, keys);
	}

	/* Clean up */
	free(keys);
	data_save();

	return EXIT_SUCCESS;

exit_error:
	free(keys);
	data_save();

	return EXIT_FAILURE;
}

int main(int argc, char **argv)
{
	int a;
	int b = 0;
	int command = 0;
	char *image_path = NULL;
	char *json_path = NULL;
	char **keys;

	if (argc == 1) {
		printf("No command line arguments given. Exiting...\n");
		return EXIT_SUCCESS;
	}

	static struct option long_opts[] = {
		{"add-key",   required_argument, 0, 'a'},
		{"del-key",   required_argument, 0, 'd'},
		{"image",     required_argument, 0, 'i'},
		{"json-path", required_argument, 0, 'j'},
		{"search",    required_argument, 0, 's'},
		{"list-keys", no_argument,       0, 'l'},
		{"help",      no_argument,       0, 'h'},
		{0, 0, 0, 0}
	};

	while (optind < argc) {
		if ((a = getopt_long(argc, argv, "a:d:i:j:slh", long_opts, &b)) != -1) {
			switch (a) {
			case 'a':
				command = ADD_KEY;

				keys = get_keys(argc, argv);

				break;
			case 'd':
				command = DEL_KEY;

				keys = get_keys(argc, argv);

				break;
			case 'i':
				image_path = optarg;

				break;
			case 'j':
				json_path = optarg;

				break;
			case 's':
				command = SEARCH;

				break;
			case 'l':
				command = LIST_KEYS;

				break;
			case 'h':
				command = HELP;

				break;
			default:
				printf("Unexpected flag during argparsing: %c\n",
					a);
				abort();
			}
		} else { /* getopt returned -1, meaning it reached the end */
			break;
		}
	}

	if (command == ADD_KEY) {
		return add_keys(image_path, keys, json_path);
	} else if (command == DEL_KEY) {
		return del_keys(image_path, keys, json_path);
	} else if (command == LIST_KEYS) {
		return list_keys(image_path, json_path);
	} else if (command == SEARCH) {
		printf("Searching is unimplemented. Nothing to do.\n");
	} else if (command == HELP) {
		printf("Usage: %s -i $IMAGE_PATH [--add-key key...]|[--del-key key...]\n", argv[0]);
	} else {
		printf("Nothing to do.\n");
	}

	return EXIT_SUCCESS;
}
