/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <check.h>
#include <stdlib.h>
#include <json-c/json_object.h>

#include "data.h"
#include "json-file.h"
#include "json-image.h"

START_TEST(test_json_file_mm)
{
	struct jsonfile *jf;

	jf = json_file_init("test_imgfind.json");

	ck_assert_int_eq(json_file_free(jf), EXIT_SUCCESS);
}
END_TEST

// Before this can be properly used, JSON image deletion needs to be implemented
/*START_TEST(test_add_image)
{
	struct jsonfile *jf;
	char *keys[3] = {"asdf", "fdsa", NULL};
	int idx = 0;

	jf = json_file_init("test_imgfind.json");

	ck_assert_int_eq(add_image(jf, "image.jpg", keys), EXIT_SUCCESS);

	for (idx = 0; idx < 2; idx++)
		ck_assert(strcmp(data_get_image_keys(jf, "image.jpg")[idx], keys[idx]) == 0);

	ck_assert_int_eq(json_file_free(jf), EXIT_SUCCESS);
}
END_TEST*/

START_TEST(test_json_file_data)
{
	struct jsonfile *jf;
	char *jsondata = "{ \"images\": [ { \"path\": \"image.jpg\", \"keys\": [ \"asdf\", \"fdsa\" ] } ] }";
	char *keys[3] = {"asdf", "fdsa", NULL};
	int idx = 0;

	jf = json_file_init("test_imgfind.json");

	ck_assert(jf != NULL);

	// Programmatically add data
	data_append_keys("image.jpg", keys);

	// Make sure the jsonfile struct has all the proper data
	ck_assert(strcmp(json_object_to_json_string(jf->jo), jsondata) == 0);
	ck_assert(strcmp(jf->i[0]->path, "image.jpg") == 0);
	ck_assert(jf->size == 1);

	while (keys[idx]) {
		ck_assert(strcmp(data_get_image_keys("image.jpg")[idx], keys[idx]) == 0);
		idx++;
	}

	// Remove changes
	data_del_keys("image.jpg", keys);

	ck_assert_int_eq(json_file_free(jf), EXIT_SUCCESS);
}
END_TEST

START_TEST(test_image_exists)
{
	struct jsonfile *jf;

	jf = json_file_init("test_imgfind.json");

	ck_assert(json_image_exists(jf->jo, "image.jpg"));

	ck_assert_int_eq(json_file_free(jf), EXIT_SUCCESS);
}
END_TEST

int main(void)
{
	Suite *s1 = suite_create("json-file");
	TCase *tc1_1 = tcase_create("json-file");
	SRunner *sr = srunner_create(s1);
	int nf;

	tcase_add_test(tc1_1, test_json_file_mm);
//	tcase_add_test(tc1_1, test_add_image);
	tcase_add_test(tc1_1, test_json_file_data);
	tcase_add_test(tc1_1, test_image_exists);
	suite_add_tcase(s1, tc1_1);

	srunner_run_all(sr, CK_ENV);
	nf = srunner_ntests_failed(sr);
	srunner_free(sr);

	return nf == 0 ? 0 : 1;
}
