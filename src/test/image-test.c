/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <check.h>
#include <stdlib.h>

#include "image.h"

START_TEST(test_image_mm)
{
	struct image *img;
	char *keys[3] = {"asdf", "fdsa", NULL};

	img = image_init("image.jpg", keys);

	ck_assert(img != NULL);
	ck_assert_str_eq(img->path, "image.jpg");

	ck_assert_int_eq(image_free(img), EXIT_SUCCESS);
}
END_TEST

int main(void)
{
	Suite *s1 = suite_create("image");
	TCase *tc1_1 = tcase_create("image");
	SRunner *sr = srunner_create(s1);
	int nf;

	tcase_add_test(tc1_1, test_image_mm);
	suite_add_tcase(s1, tc1_1);

	srunner_run_all(sr, CK_ENV);
	nf = srunner_ntests_failed(sr);
	srunner_free(sr);

	return nf == 0 ? 0 : 1;
}
