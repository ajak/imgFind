/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <check.h>
#include <stdlib.h>

#include "datastructs.h"

START_TEST(test_exists)
{
	struct ll_node *head = ll_create();

	// Test for nonexistence on null list
	ck_assert(!ll_exists(head, "fdsa"));

	// Test on list with single node
	ck_assert(ll_insert(head, "asdf") == EXIT_SUCCESS);
	ck_assert(ll_exists(head, "asdf"));

	// Test on list with multiple nodes
	ck_assert(ll_insert(head, "fdsa") == EXIT_SUCCESS);
	ck_assert(ll_exists(head, "fdsa"));

	// Test on list with data of similar leading characters
	ck_assert(!ll_exists(head, "fd"));

	// Clean up
	ck_assert(ll_free(head) == EXIT_SUCCESS);
}
END_TEST

START_TEST(test_insert)
{
	struct ll_node *head = ll_create();
	struct ll_node *node = NULL;

	// Test on null node
	ck_assert(ll_insert(head, "asdf") == EXIT_SUCCESS);
	ck_assert(strcmp((char*)head->data, "asdf") == 0);
	ck_assert(head->next == NULL);

	// Test on tail node
	ck_assert(ll_insert(head, "fdsa") == EXIT_SUCCESS);

	// node will point to the second list object, "fdsa"
	node = head->next;

	ck_assert(strcmp((char*)node->data, "fdsa") == 0);
	ck_assert(node->next == NULL);

	// Test on non-tail/non-null node
	ck_assert(ll_insert(node, "sdaf") == EXIT_SUCCESS);

	// New node should be inserted after given node
	ck_assert(strcmp((char*)node->next->data, "sdaf") == 0);
	ck_assert(node->next->next == NULL);

	ll_free(head);
}
END_TEST

START_TEST(test_delete)
{
	struct ll_node *head = ll_create();

	/* Here we need to test 5 cases - deleting middle, tail, head, nodes of
	   lists with one node, and a fail-test should the given data not exist
	   in the list. */
	ck_assert(ll_insert(head, "asdf") == EXIT_SUCCESS);
	ck_assert(ll_insert(head, "fdsa") == EXIT_SUCCESS);
	ck_assert(ll_insert(head, "sdaf") == EXIT_SUCCESS);
	ck_assert(ll_insert(head, "afsd") == EXIT_SUCCESS);

	// Order of the list should be "asdf", "afsd", "sdaf", "fdsa"

	// Delete node that shouldn't exist
	ck_assert(ll_delete(head, "jkl;") == head);

	// Delete a middle
	head = ll_delete(head, "afsd");

	// Order should be "asdf", "sdaf", "fdsa"
	ck_assert(strcmp(head->data, "asdf") == 0);
	ck_assert(head->next->next->next == NULL);

	// Delete a tail
	head = ll_delete(head, "fdsa");

	// Order should be "asdf, "sdaf"
	ck_assert(strcmp(head->data, "asdf") == 0);
	ck_assert(head->next->next == NULL);

	// Delete a head
	head = ll_delete(head, "asdf");

	// Order should be "sdaf"
	ck_assert(strcmp(head->data, "sdaf") == 0);
	ck_assert(head->next == NULL);

	// Delete the last node
	head = ll_delete(head, "sdaf");

	// No freeing necessary since all nodes were freed in the deletions
}
END_TEST

int main(void)
{
	Suite *s1 = suite_create("linkedlist");
	TCase *tc1_1 = tcase_create("linkedlist");
	SRunner *sr = srunner_create(s1);
	int nf;

	suite_add_tcase(s1, tc1_1);
	tcase_add_test(tc1_1, test_exists);
	tcase_add_test(tc1_1, test_insert);
	tcase_add_test(tc1_1, test_delete);

	srunner_run_all(sr, CK_ENV);
	nf = srunner_ntests_failed(sr);
	srunner_free(sr);

	return nf == 0 ? 0 : 1;
}
