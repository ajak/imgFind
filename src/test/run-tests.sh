#!/bin/sh

echo

# $1 is passed by the all target of the Makefile in this dir
for FILE in $1
do
	CK_FORK=no valgrind --leak-check=full --xml=yes --xml-file=$FILE.xml ./$FILE
done

echo

# Two loops to separate output
for FILE in $1
do
	count=$(cat $FILE.xml | grep "<error>" | wc -l)

	if [ $count -gt 0 ]; then
		echo $count "memory management errors in $FILE. Details in src/test/$FILE.xml"
	fi
done

echo
