/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <check.h>
#include <stdlib.h>

#include "data.h"
#include "json-file.h"
#include "main.h"

struct opts o;
struct jsonfile *jf;

START_TEST(test_data_load)
{
	o.jf_path = "test_imgfind.json";

	ck_assert(data_load() == EXIT_SUCCESS);

	ck_assert(jf);
}
END_TEST

START_TEST(test_data_append_keys)
{
	char *keys[3] = {"asdf", "fdsa", NULL};
	int idx;

	jf = json_file_init("test_imgfind.json");

	ck_assert_int_eq(data_append_keys("image.jpg", keys), EXIT_SUCCESS);

	for (idx = 0; idx < 2; idx++)
		ck_assert(strcmp(data_get_image_keys("image.jpg")[idx], keys[idx]) == 0);
}
END_TEST

START_TEST(test_data_del_keys)
{
	char *keys[3] = {"asdf", "fdsa", NULL};
	char *keyA[2] = {"asdf", NULL};
	char *keyB[2] = {"fdsa", NULL};
	int idx;

	// Test deleting everything
	data_append_keys("image.jpg", keys);

	ck_assert_int_eq(data_del_keys("image.jpg", keys), EXIT_SUCCESS);
	ck_assert(data_get_image_keys("image.jpg") == NULL);

	// Test deleting keys in order of the list
	data_append_keys("image.jpg", keys);

	ck_assert_int_eq(data_del_keys("image.jpg", keyA), EXIT_SUCCESS);
	ck_assert(data_get_image_keys("image.jpg") == keyB);

	ck_assert_int_eq(data_del_keys("image.jpg", keyB), EXIT_SUCCESS);
	ck_assert(data_get_image_keys("image.jpg") == NULL);
}
END_TEST

START_TEST(test_data_list_keys)
{
	char *keys[3] = {"asdf", "fdsa", NULL};
	int idx;

	data_append_keys("image.jpg", keys);

	for (idx = 0; idx < 2; idx++)
		ck_assert(strcmp(data_get_image_keys("image.jpg")[idx], keys[idx]) == 0);

	data_del_keys("image.jpg", keys);
}
END_TEST

START_TEST(test_data_save)
{
	ck_assert(data_save() == EXIT_SUCCESS);

//	ck_assert(!jf);
}
END_TEST

int main(void)
{
	Suite *s1 = suite_create("data");
	TCase *tc1_1 = tcase_create("data");
	SRunner *sr = srunner_create(s1);
	int nf;

	tcase_add_test(tc1_1, test_data_load);
	tcase_add_test(tc1_1, test_data_append_keys);
	tcase_add_test(tc1_1, test_data_del_keys);
	tcase_add_test(tc1_1, test_data_list_keys);
	tcase_add_test(tc1_1, test_data_save);

	suite_add_tcase(s1, tc1_1);

	srunner_run_all(sr, CK_ENV);
	nf = srunner_ntests_failed(sr);
	srunner_free(sr);

	return nf == 0 ? 0 : 1;
}

