/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <check.h>

#include "util.h"

START_TEST(string_contains_test)
{
	char *test_array[5] = {"a", "b", "c", "abc", NULL};

	fail_unless(string_contains("a", test_array)==1, "string_contains in util.h failed");
	fail_unless(string_contains("b", test_array)==1, "string_contains in util.h failed");
	fail_unless(string_contains("c", test_array)==1, "string_contains in util.h failed");
	fail_unless(string_contains("abc", test_array)==1, "string_contains in util.h failed");

	fail_unless(string_contains("z", test_array)==0, "string_contains in util.h failed");
}
END_TEST

int main(void)
{
	Suite *s1 = suite_create("util");
	TCase *tc1_1 = tcase_create("util");
	SRunner *sr = srunner_create(s1);
	int nf;

	suite_add_tcase(s1, tc1_1);
	tcase_add_test(tc1_1, string_contains_test);

	srunner_run_all(sr, CK_ENV);
	nf = srunner_ntests_failed(sr);
	srunner_free(sr);

	return nf == 0 ? 0 : 1;
}
