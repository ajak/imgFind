This is in early development and is currently not functional.

# Usage

An image always needs to be specified. This is done with the `--image` option,
shortened to `-i`.

```sh
~ $ imgFind --image image_path
Nothing to do.
```

## Adding Keys

Any number of keys can be added at once, using the option `--add-keys`, or
`-a`.

```sh
~ $ imgFind --add-key keys... --image image_path
```

## Deleting Keys

Any number of keys can be deleted at once, using the option `--del-key`, or
`-d`.

```sh
~ $ imgFind --del-key keys... --image image_path
```

## Listing Keys

This can be done with `--list-keys` or `-l`.

```sh
~ $ imgFind --add-key key yek --image image.jpg
~ $ imgFind --list-keys -i image.jpg
key
yek
```

# Dependencies

This program currently depends on libc, libexif, json-c, and libmagic.

# Tests

Tests can be run by running the command `make test`. This builds the project
as well as the tests and runs the tests. The test results are available near the
end of the output of make, as well as a count of memory management errors per
test binary. The details of the errors can be found in the src/test directory in
an XML file of the same name as the binary.

# License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
